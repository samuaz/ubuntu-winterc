FROM ubuntu:disco as builder
ENV CLANGD_VERSION="8"
ENV GCC_VERSION="9"
ENV OPENSSL_VERSION=1.0.2
ENV GRPC_VERSION=v1.24.x
ENV PROTO_VERSION=3.10.x
ENV MYSQL_VERSION=8.0.17
ENV MYSQL_CON_VERSION=8.0.17
ENV MYSQL_PLATFORM=linux-glibc2.12
ENV MYSQL_EXTENSION=tar.xz
ENV MYSQL_EXTRACT_COMMAND=xf
ENV APT_KEY_DONT_WARN_ON_DANGEROUS_USAGE=DontWarn
ENV WINTER_VERSION=0.1

COPY . .

RUN  apt-get update -y &&  \
 apt-get upgrade -y &&  \
 apt-get dist-upgrade -y

RUN apt-get -y install software-properties-common

RUN apt-get install -y \
    apt-utils gcc g++ openssh-server cmake build-essential gdb gdbserver rsync vim

RUN apt-get install -y apt-transport-https ca-certificates gnupg software-properties-common wget

#RUN wget --no-check-certificate -qO - https://apt.kitware.com/keys/kitware-archive-latest.asc 2>/dev/null | apt-key add -
#RUN apt-add-repository 'deb https://apt.kitware.com/ubuntu/ bionic main'
#RUN  apt-get update -y
#RUN  apt-get -y install cmake

RUN  wget https://apt.llvm.org/llvm.sh --no-check-certificate && \
 chmod +x llvm.sh && \
 ./llvm.sh $CLANGD_VERSION

#RUN add-apt-repository ppa:ubuntu-toolchain-r/ppa -y
RUN apt-get update -y &&  \
 apt-get install build-essential software-properties-common -y && \
 apt-get install gcc-$GCC_VERSION g++-$GCC_VERSION -y && \
 update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-$GCC_VERSION 60 --slave /usr/bin/g++ g++ /usr/bin/g++-$GCC_VERSION && \
 update-alternatives --config gcc

RUN  apt-get -y install libfuzzer-$CLANGD_VERSION-dev
RUN  apt-get -y install libllvm-$CLANGD_VERSION-ocaml-dev libllvm$CLANGD_VERSION llvm-$CLANGD_VERSION llvm-$CLANGD_VERSION-dev llvm-$CLANGD_VERSION-doc llvm-$CLANGD_VERSION-examples llvm-$CLANGD_VERSION-runtime
RUN  apt-get -y install libc++-$CLANGD_VERSION-dev libc++abi-$CLANGD_VERSION-dev
RUN  apt-get -y install libomp-$CLANGD_VERSION-dev clang-$CLANGD_VERSION-doc libomp-$CLANGD_VERSION-doc llvm-$CLANGD_VERSION-doc

RUN  apt-get install g++ gcc make gdb gdbserver clang-tools-${CLANGD_VERSION} -y && \
     apt-get clean && \
     apt-get -y autoremove && \
     rm -rf /var/lib/apt/lists/* && \
     ln -s /usr/bin/clangd-${CLANGD_VERSION} /usr/bin/clangd

RUN apt-get update && apt-get -y install g++ && \
         apt-get -y install build-essential && \
         apt-get -y install autotools-dev && \
         apt-get -y install automake && \
         apt-get -y install autoconf && \
         apt-get -y install cmake && \
         apt-get -y install libboost-all-dev && \
         apt-get -y install libssl-dev && \
         apt-get -y install libncurses5-dev libncursesw5-dev && \
         apt-get -y install libtool && \
         apt-get -y install curl && \
         apt-get -y install openssl libssl-dev && \
         apt-get -y install zlib1g-dev && \
         apt-get -y install git && \
         curl https://www.openssl.org/source/openssl-$OPENSSL_VERSION.tar.gz | tar xz && cd openssl-$OPENSSL_VERSION &&  ./config --prefix=/usr/local/ssl --openssldir=/usr/local/ssl shared &&  make &&  make install

RUN mkdir libs
RUN mkdir protobufout
RUN mkdir mysql
RUN mkdir redis
run mkdir yaml
WORKDIR "/libs"

RUN wget https://github.com/protocolbuffers/protobuf/releases/download/v$PROTO_VERSION/protobuf-all-$PROTO_VERSION.tar.gz --no-check-certificate && \
    tar -xvzf protobuf-all-$PROTO_VERSION.tar.gz && \
    cd protobuf-$PROTO_VERSION && \
    ./configure --prefix=/protobufout && \
    make # && \
    #make install && \
    #ldconfig

RUN git clone -b $GRPC_VERSION https://github.com/grpc/grpc && \
    cd grpc && \
    git submodule update --init && \
    make CFLAGS='-Wno-ignored-qualifiers -w' CXXFLAGS='-Wno-ignored-qualifiers -w' # && \
    #make install

RUN cd .. && \
    git clone https://github.com/jbeder/yaml-cpp.git && \
    cd yaml-cpp && \
    mkdir build && \
    cd build && \
    cmake .. -DCMAKE_INSTALL_PREFIX:PATH=/yaml && \
    make  && \
    make install

RUN cd .. && \
    git clone https://github.com/Cylix/cpp_redis.git && \
    cd cpp_redis  && \
    git submodule init && git submodule update  && \
    mkdir build && cd build  && \
    cmake .. -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX:PATH=/redis && \
    make && \
    make install

RUN cd .. && \
    git clone https://github.com/mysql/mysql-connector-cpp.git && \
    cd mysql-connector-cpp && \
    git submodule update --init && \
    mkdir build && \
    cd build  && \
    wget https://dev.mysql.com/get/Downloads/MySQL-8.0/mysql-$MYSQL_VERSION-$MYSQL_PLATFORM-x86_64.$MYSQL_EXTENSION --no-check-certificate && \
    tar -$MYSQL_EXTRACT_COMMAND mysql-$MYSQL_VERSION-$MYSQL_PLATFORM-x86_64.$MYSQL_EXTENSION && \
    cp -rf mysql-$MYSQL_VERSION-$MYSQL_PLATFORM-x86_64/include/* /usr/local/include/ && \
    cp -rf mysql-$MYSQL_VERSION-$MYSQL_PLATFORM-x86_64/lib/* /usr/local/lib/ && \
    cmake -DMYSQL_CXXFLAGS=-stdlib=libc++ -DCMAKE_INSTALL_LIBDIR=/usr/local/lib/ -DWITH_SSL=/usr/local -DMYSQL_LIB_DIR=mysql-${MYSQL_VERSION}-$MYSQL_PLATFORM-x86_64/lib -DMYSQL_INCLUDE_DIR=mysql-${MYSQL_VERSION}-$MYSQL_PLATFORM-x86_64/include -DCMAKE_BUILD_TYPE=Release -DWITH_JDBC=TRUE -DBUILD_STATIC=ON -DCMAKE_INSTALL_PREFIX:PATH=/mysql .. && \
    make  && \
    make install && \
    #buildconfig

#RUN cd .. && \
#    git clone -b release/$WINTER_VERSION https://samuaz@bitbucket.org/theopencompany/winter.git && \
#    cd winter && \
#    mkdir build && \
#    cd build && \
#    cmake .. && \
#    make install && \
#    ldconfig

#WORKDIR "/libs"
RUN rm -rf *
